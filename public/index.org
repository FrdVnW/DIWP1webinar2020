#+TITLE: Insight into actors' worldviews on crop diversification experiences across Europe
#+Author: Frédéric Vanwindekens, Louise Legein (CRA-W)
#+Email: f.vanwindekens@cra.wallonie.be
#+Date: DiverIMPACTS Webinar - 22/01/2020
# Sharing slides : 
# [[https://frdvnw.gitlab.io/DIWP1webinar2020]]

#+STARTUP: overview
#+STARTUP: hidestars
#+STARTUP: logdone
#+COLUMNS: %38ITEM(Details) %7TODO(To Do) %TAGS(Context) 
#+OPTIONS: tags:t 
#+OPTIONS: timestamp:t p:t
#+OPTIONS: todo:t
#+OPTIONS: TeX:t
#+OPTIONS: LaTeX:t          
#+OPTIONS: skip:t @:t ::t |:t ^:t f:t
#+OPTIONS: reveal_single_file:t
#+REVEAL_INIT_OPTIONS: width:1200, height:800, controlsLayout: 'bottom-right', slideNumber: true, transition:'fade'

# Comment for LaTeX
#+OPTIONS: num:nil toc:nil
#+EXCLUDE_TAGS: comments

#+REVEAL_MARGIN: 0.1
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_EXTRA_CSS: ./frd_budapest.css
#+REVEAL_PLUGINS: (markdown notes)
# +PROPERTY: results output
# +PROPERTY: exports both

#+LaTeX_CLASS: koma-article
# #+LATEX_HEADER: \usepackage[margin=0.5in]{geometry}

#+PROPERTY: header-args:R :exports both :session *R-webinar* :tangle yes

#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Webinar DiverIMPACTS - WP1 - Insight into actors' worldviews on crop diversification experiences across Europe">
#+REVEAL_POSTAMBLE: <p> Created by FrdVnw </p>

#+latex_header: \input{preambule.tex}


* Announcement                                                     :comments:
Within the mainstream socio-technical landscape of European farming systems, farmers experience crop diversification, often in association with other actors like manufacturers, retailers, consumers. In DiverIMPACTS, we aim to document and understand the diversity and the complexity of these past or present experiences. After an extensive quantitative on-line survey, our partners conducted in-depth interviews of main actors of a selected number of crop diversification experiences. In this webinar we will present both (i) the original analytical approach based on qualitative data coding and cognitive mapping development, and (ii) the results of our research, highlighting key concepts of actors' worldviews, success factors or failure, drivers and barriers within pathways to crop diversification. 

During the webinar, we will illustrate the mapping process based on participants' worldviews on crop diversification around questions of drivers and barriers linked to their own experiences, cases studies or knowledge.

* Context
** Social-ecological systems
Integrated system of ecosystems and human society with reciprocal feedback and
interdependence. The concept emphasizes the humans-in-nature perspective 

#+ATTR_HTML: :style text-align:right
#+ATTR_HTML: :style font-size:50%
Walker, 2010

** Social-ecological systems
[[./images/agriculture.jpg]]
** Social-ecological systems
   [[./images/forest.jpg]]
** Social-ecological systems
 [[./images/fisheries.jpg]]

** Social-ecological systems
#+html: <p></p>

|-------------------------------------------------+----------------------------|
| Techniques                                      | Practices                  |
|-------------------------------------------------+----------------------------|
| group of operations aiming to produce an effect | way of applying techniques |
| grounded in the knowledge                       | grounded in the action     |
|-------------------------------------------------+----------------------------|

#+ATTR_HTML: :style text-align:right
#+ATTR_HTML: :style font-size:50%
Blanc-Pamard & Milleville, 1985 ; Landais, 1988

#+REVEAL: split
[[./images/worldviews.png]]

# +REVEAL_DATA_TRANSITION: slides

** Surveying stakeholders of food systems
#+html: <p></p>

#+ATTR_HTML: :style font-size:75%
|------------+-------------------------------+-----------------------------------|
|            | Quantitative  survey          | Qualitative survey                |
|------------+-------------------------------+-----------------------------------|
| collection | paper / on-line surveys       | face-to-face interviews, recorded |
| questions  | closed                        | open-ended                        |
| answers    | numerical, factorial, ordinal | speech                            |
| data       | data base, data  frames       | texts, transcriptions, notes      |
| analyses   | data          analysis        | qualitative coding                |
| outputs    | contigency    tables,   plots | monography                        |
|------------+-------------------------------+-----------------------------------|

** Data science
[[./images/data_science.png]]

* The approach : qualitative survey & cognitive mapping
#+BEGIN_NOTES
Qualitative interviews were thus planned in order to catch the worldviews of stakeholders on successful crop diversification :
#+END_NOTES

** an approach for documenting systems of practices
    [[./images/mm_socmap_pap1.png]]

    #+ATTR_HTML: :style font-size:50%
    Vanwindekens, F.M., Stilmant, D. & Baret, P.V. (2013). Development of a broadened cognitive mapping approach for analysing systems of practices in social-ecological systems. Ecological Modelling 250 : 352-362.

** Step 1 : collecting data
*** an online training
# an online training was made available for the partners in charge of the interviews
   - use a common method
   - produce quality material
   - avoid common mistakes
*** the open-ended interview itself
  - Common (but flexible) grid
  - based on
    1. DiverIMPACTS research question
    2. and on the results of the first survey
  - 53 interviews in total conducted in 11 countries

*** after the interview
   - each partner transcribed and translated the interviews in English
   - then sent to Fred and Louise for coding
   - transformation by Fred and Louise into appropriate .txt files and introduced into qcoder

Before the coding itself...

** Step 2 : coding data with qcoder
   #+ATTR_HTML: :style font-size:50%
   forked from =github.com/ropenscilabs:qcoder= (Elin Waring et al., 2018)
#+REVEAL_HTML: <iframe width="1120" height="630" data-src="https://www.youtube.com/embed/bQ8xfwe5NPU?t=3"></iframe>
** Step 3 : Individual mapping
[[./images/cogmap1.png]]

** Step 4 : Social mapping
[[./images/cogmap2.png]]

** Functionalities : only edges with weight > 3
[[./images/cogmap3.png]]
** Ex. ggplot2 version
[[./images/cogmap4.png]]
** Analysis of one concept
[[./images/cogmap5.png]]
** Analysis of one concept, highlighting
[[./images/cogmap6.png]]

** Analysis of one concept, filtered
[[./images/cogmap7.png]]
** Saturation of the topic
#+ATTR_HTML: :style width:75%
[[./images/saturation.png]]
** Comparing groups of documents
#+HTML: <p>
#+ATTR_HTML: :style font-size:75%
#+CAPTION:Relationships showing significant differences between groups of actors
|---------------+------------|
| Relationships |    p.value |
|---------------+------------|
| 1~2           | 0.00591152 |
| 1~25          |  0.0187574 |
| 15~1          | 0.00591152 |
| 25~39         |  0.0223372 |
| 25~59         |  0.0223372 |
| 28~25         | 0.00884127 |
| 59~25         |  0.0223372 |
| 6~17          |  0.0144928 |
|---------------+------------|

* Analyses of key factors of crop diversification (D1.3)
Our research aim to highlight key concepts of actors' worldviews, success factors or failure, drivers and barriers within pathways to crop diversification
** Agronomic innovation
#+HTML: <p>
#+ATTR_HTML: :style font-size:75%
#+CAPTION:Concept centrality of success factors
|-----------------+------------|
| Concepts        | Centrality |
|-----------------+------------|
| Intercropping   |         62 |
| Crop production |         55 |
| Knowledge       |         43 |
| Cover crops     |         42 |
| Organic         |         36 |
| Profit          |         35 |
| Farmer          |         34 |
| Grain legumes   |         31 |
| Rotations       |         30 |
| Weed            |         25 |
|-----------------+------------|

** Agronomic innovation
#+HTML: <p>
#+ATTR_HTML: :style font-size:75%
#+CAPTION:Concept centrality of failure factors
|-----------------+------------|
| Concepts        | Centrality |
|-----------------+------------|
| Profit          |         43 |
| Crop production |         31 |
| Soil            |         27 |
| Chemical inputs |         23 |
| Intercropping   |         23 |
| Weed            |         21 |
| No-till         |         19 |
| Pests           |         18 |
| Markets         |         17 |
| Rotations       |         16 |
|-----------------+------------|

*** highlighting relationships between actors

 [[./images/Social_rel.png]]

 #+BEGIN_NOTES
 *Notes*
 - in the quantitative survey, one of the hypothesis that  emerged from the quantitative survey was that ="Relationships between value chain (VC) actors contributes to the success of a CDE"=. In the qualitative survey, we can also observe that those relationships are well represented
 - Farmers in the center strong exchanges with other farmers 
 - (+): knowledge or machinery exchanges (lever to machinery costs linked to diversification), cooperation to have more influence over the downstream players (to make the processors come to the region to sort the harvest)
 - (+) also exchanges within farmer groups that are facilitated by advisors
 BUT :
 - (-) difficult to get along with neighbours when they do not understand why you are more innovative
 - (-) some farmers complain that downstream players take all the benefits and keep them away from the consumers
 "Policy makers often think that farmers are stupid"  (farmer)
 #+END_NOTES

*** Other highlights
 - Farmers are both receivers and source of knowledge
#+BEGIN_QUOTE
But an old farmer may know a lot more than a researcher [...]. But an old farmer has no authority in that manner, because he has no degree. It is thought that he does not know how to manage his soil. This just is not true. (farmer)
#+END_QUOTE

*** Other highlights
 - they acquire knwoledge from experience, on-farm trials, magazines, on the internet, ...
 - Organic agriculture tend to diversify out of necessity
   1. production of legumes for fertilization
   2. extended rotations to prevent weed, pests and diseases
 - Economic interest of organic farming but several constraints
   1. administrative
   2. organization of the VC
   3. agronomic challenges

 #+BEGIN_NOTES
 *Notes* 
 - administrative : ex: bureaucracy, regulations
 - organization of the VC : ex: separate storage for crops, multiplied with C2 and multiple cropping
 - agronomic challenges : ex: pests on grain legumes, hand weeding, ...
 #+END_NOTES

*** Other highlights
 - bad prices, price falls and high production costs are barriers to diversification
 - New practices requires new investments
   1. sorting
   2. new varieties
   3. Machinery (especially in CA)
 - Some policies may encourage certain practices but may also hinder others. In addition, several farmers expressed the feeling that European decision-makers were too disconnected from reality.

*** The farmers interviewed can be classified into 3 types
- organic farmers
- 'Conservation' farmers
- farmers involved in Water Protection Areas

** Value-chain innovation 

#+HTML: <p>
#+ATTR_HTML: :style font-size:75%
#+CAPTION:Most central concepts connected to success practices
| Concepts          | Centrality |
|-------------------+------------|
| Manufacturers     |         49 |
| Innovations       |         45 |
| Knowledge         |         44 |
| Rotations         |         41 |
| Coorperative      |         37 |
| Farmer            |         32 |
| Legumes           |         30 |
| Soil              |         26 |
| Profit            |         21 |
| Farming practices |         21 |

** Value-chain innovation 

#+HTML: <p>
#+ATTR_HTML: :style font-size:75%
#+CAPTION:Most central concepts connected to risk of failure
| Concepts       | Centrality |
|----------------+------------|
| Pests          |         17 |
| Weather        |         13 |
| Machinery      |         12 |
| Manufacturers  |         11 |
| Markets        |         11 |
| Hemp           |         11 |
| Yield - Growth |         11 |
| Scales         |          9 |
| Legumes        |          8 |
| Risk           |          8 |

*** Most central concepts have been classed among six main groups of concepts
- ecological factors
- economical factors
- farming (technical, agronomical)
- players (actors, stakeholders, farmers, social interaction)
- 'vision'
- case-specific concepts

#+BEGIN_NOTES
For the treatment of information, the 30 most central concepts have been classed in six main groups : 
\begin{itemize}
\item "ecological", with the concepts of Weather and Soil
\item "economical", with the concepts of Profit, Markets, Prices and Fund
\item "farming" (technical, agronomical), with the concepts of Rotations, Pests, Machinery, Fertilisation, Yield - Growth and Farming practices
\item "players" (interaction, social), with the concepts of Authorities, Other farmers, Manufacturers, Retailers, Consumers and Farmer
\end{itemize}

These four first groups are \emph{ex ante} defined group of concepts, identified before the coding step. Two more groups have been identified during analyses of results :

\begin{itemize}
\item "vision", a broad category with the concepts of Knowledge, Diversity, Innovations, New products and Sustainable production
\item "specificity", linked to the specific experiences, with the concepts of Organic Farm, Hemp, Bio Energy Project, Sugar beets, Row crops and Legumes
\end{itemize}
#+END_NOTES

*** Mapping the main economical concepts

 #+CAPTION: Highlighted social cognitive map around the economical concepts
 file:./images/path_econo.png

*** Profits & profitability

 #+BEGIN_QUOTE
 Some legumes have also been introduced such as bean and pea to increase soil fertility even if they are not very profitable crops from an economic point of view.
 #+END_QUOTE

  #+BEGIN_QUOTE
 Yes, for me, it's a success. Because it brings me not just a direct profitability. The objective initially was to put agronomy back on our farms. It has always been but it is very important. Yes, it's a very good production. It’s a protein crop, it’s a spring crop. Even if it is a crop that is riskier. I think in 10 years we still have enough profitability. With very good years, others not so good, must do with. It's the climatic hazards.
  #+END_QUOTE

 #+BEGIN_NOTES
 Profit is mainly a receiver concept, influenced by the Markets and the Consumers, but also by Innovations and New products. In this group of experiences, the influence of new crops in rotations (Legumes, among others) is not always seen as positive, as initially implemented for an agronomic aim.

 But, most of the time, diversification is associated with profitability, on the long term.
 #+END_NOTES

*** Consumers

#+BEGIN_QUOTE
We are trying to give a good price to farmers, but then, we will not sell the product in the country. Nobody will buy pickled cucumbers 4 times more expensive than conventional ones. Unfortunately, the goods are moving abroad.
#+END_QUOTE

#+BEGIN_QUOTE
Well, customers are fans of yogurt, for example, everybody loves that. But the small farmer, in his farm, is not going to make Greek yoghurt, he could not. Everybody wants mozzarella. Well, here we have local buffalo mozzarella, which is delicious but very expensive. We do not have 1-euro mozzarella balls. That is what people buy, though. That comes from abroad, it’s a pity. 
 #+END_QUOTE

#+BEGIN_NOTES
The Consumers and their choice of consumptions have a direct impact on the profitability of the experience. They are, themselves, influenced by the Prices of the products derived from the crop diversification.
#+END_NOTES

*** Prices and costs

#+BEGIN_QUOTE
If you know that it is a risky crop and that you won’t harvest one year out of five, the price must be increased by 20% to take this risk into account. It is difficult to assert that, to defend that, but it is how it should work because…
#+END_QUOTE

#+BEGIN_QUOTE
There are no official subsidies for organic processing. We are treated in the same way as a conventional processing plant and we cannot obtain any other forms of assistance. We have to buy very expensive vegetables and fruits, because the farmer is used to good prices offered by western companies. Western companies often do not sell their products here, but they do buy raw material. [...] Our problems start here and we have to pay as much as the competition. 
#+END_QUOTE

#+BEGIN_NOTES
Prices of product coming from iversification strategies are determined by the Risk associated with the innovation.

The differences between European countries in term of labour costs and the products prices for consumers can have a negative influence on the product prices in Eastern Countries.

Markets is another central concept of the economical sub-map. Markets logically influence the choices of the crops to include in the rotations. Having a market for a product is an essential precondition for launching a sustainable experience linked to a value chain innovation.
#+END_NOTES

*** Other highlights on the other dimensions 
 - Weather :: (-) abnormalities or extreme events
 - Soil :: (+) impacted by diversified practices
 - Manufactures are central, farmers/.../retailers/consumers. Requiring quality products, source of innovations.
 - Consumers :: influence on the system by their choices
 - Cooperative :: tasks individual farmers could not perform, linked to economic or technical concepts
 #+BEGIN_NOTES
- Weather is evoked by main actors as a pure transmitter concept, as having impacts on Yield and Growth, Pests and Work organisation. Most of the quotes are connected to failure, due to abnormalities or extreme events.
- The soil is mostly evoked as a receiver concept, as positively impacted by the successful Farming practices, more precisely linked to Rotations and Legumes.
- Manufacturers have a highly central position in this players' sub-map, as in the complete social map (most central concept for the Value-Chain Diversification experiences). This concept act as well as a receiver concept, as crops (Legumes, Cereals) grown by farmers are sold to them, as well as a transmitter concept, selling manufactured products to Consumers, indirectly by Markets or Retailers. They can initiate Innovations and being an important source of Knowledge on diversification techniques and can have requirements on the quality and, therefore, have an impact on Farming practices.
- Consumers are important in the system. Via their choices, they can have an impact on Decisions and global orientation of experience.
- According some retailers or manufacturers, the success of experiences comes, at least partly, from the Communication to the Consumers, and the packaging could have influence.
- Cooperative is another central player for some diversification experiences, linked to a myriad of concepts, with links smaller or equal to two. These organisations have competences and take in charge tasks individual farmers could not perform. They are linked to economic concepts (Contracts, Prices, Fund, Profit, Manufacturers) and to technical concepts (Decision, Varieties, Farming practices).
#+END_NOTES

*** Other highlights on the other dimensions 
 - Rotations :: + Pests, Biodiversity, (agro-)Diversity
 - Fertilisation :: a source of innovations, new crops
 - Machinary :: constraining for some cases, advantage for others


#+BEGIN_NOTES
- Changing Crop Rotations on farm are the fruit of Innovations process, adding new crops (Vegetables, Legumes). In interviews, Rotations have been associated with benefits with regards to Pests, Biodiversity and (agro-)Diversity.
- The Weather, as previously discussed, has an impact on Pests development and on the Yield / Growth of crops.
- Fertilisation is an important concept from an agronomical point of views. Players are searching innovative ways for fertilizing crops, limiting the use of chemical fertilizers and promoting sustainable sources of nitrogen, essentially adding Legumes in the Rotations, as cash and/or service crops.
- Concerning diversification, Machinery is an element of the farming system that focuses  attention. For various crops, like Hemp or Mustard, it is still a constraint during harvests (Losses) and Innovations are required. These innovations are sometimes done by farmer themselves through On-farm trials.
- For other crops (like lentils, for example), Machinery is pointed as an advantage as they do not require specific materials for farms having already other protein crops.
 #+END_NOTES


* Let's explore your worldviews !





